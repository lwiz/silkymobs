package us.lukemontalvo.SilkyMobs;

import org.bukkit.Material;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public class SilkyMobsBlockListener implements Listener {
	@EventHandler(priority=EventPriority.NORMAL)
	public void onBlockBreak(BlockBreakEvent event) {
		if (event.getBlock().getType() == Material.SPAWNER) {
			if (
				(event.getPlayer().hasPermission("SilkyMobs.mine_any"))
				||(
					(event.getPlayer().hasPermission("SilkyMobs.mine"))
					&&(event.getPlayer().getInventory().getItemInMainHand().containsEnchantment(Enchantment.SILK_TOUCH))
				)
			) {
				ItemStack spawner_stack = new ItemStack(Material.SPAWNER, 1);

				CreatureSpawner spawner = (CreatureSpawner) event.getBlock().getState();
				Material egg_mat = getEggMaterial(spawner.getSpawnedType());
				ItemStack egg_stack = (egg_mat == null) ? null : new ItemStack(egg_mat, 1);

				event.getPlayer().getWorld().dropItemNaturally(event.getBlock().getLocation(), spawner_stack);
				if (egg_stack != null) {
					event.getPlayer().getWorld().dropItemNaturally(event.getBlock().getLocation(), egg_stack);
				}
			}
		}
	}

	private static Material getEggMaterial(EntityType type) {
		switch (type) {
			case BAT: {
				return Material.BAT_SPAWN_EGG;
			}
			case BLAZE: {
				return Material.BLAZE_SPAWN_EGG;
			}
			case CAVE_SPIDER: {
				return Material.CAVE_SPIDER_SPAWN_EGG;
			}
			case CHICKEN: {
				return Material.CHICKEN_SPAWN_EGG;
			}
			case COD: {
				return Material.COD_SPAWN_EGG;
			}
			case COW: {
				return Material.COW_SPAWN_EGG;
			}
			case CREEPER: {
				return Material.CREEPER_SPAWN_EGG;
			}
			case DOLPHIN: {
				return Material.DOLPHIN_SPAWN_EGG;
			}
			case DONKEY: {
				return Material.DONKEY_SPAWN_EGG;
			}
			case DROWNED: {
				return Material.DROWNED_SPAWN_EGG;
			}
			case ELDER_GUARDIAN: {
				return Material.ELDER_GUARDIAN_SPAWN_EGG;
			}
			case ENDERMAN: {
				return Material.ENDERMAN_SPAWN_EGG;
			}
			case ENDERMITE: {
				return Material.ENDERMITE_SPAWN_EGG;
			}
			case EVOKER: {
				return Material.EVOKER_SPAWN_EGG;
			}
			case GHAST: {
				return Material.GHAST_SPAWN_EGG;
			}
			case GUARDIAN: {
				return Material.GUARDIAN_SPAWN_EGG;
			}
			case HORSE: {
				return Material.HORSE_SPAWN_EGG;
			}
			case HUSK: {
				return Material.HUSK_SPAWN_EGG;
			}
			case LLAMA: {
				return Material.LLAMA_SPAWN_EGG;
			}
			case MAGMA_CUBE: {
				return Material.MAGMA_CUBE_SPAWN_EGG;
			}
			// for MOOSHROOM see MUSHROOM_COW
			case MULE: {
				return Material.MULE_SPAWN_EGG;
			}
			case MUSHROOM_COW: {
				return Material.MOOSHROOM_SPAWN_EGG;
			}
			case OCELOT: {
				return Material.OCELOT_SPAWN_EGG;
			}
			case PARROT: {
				return Material.PARROT_SPAWN_EGG;
			}
			case PHANTOM: {
				return Material.PHANTOM_SPAWN_EGG;
			}
			case PIG: {
				return Material.PIG_SPAWN_EGG;
			}
			case PIG_ZOMBIE: {
				return Material.ZOMBIE_PIGMAN_SPAWN_EGG;
			}
			case POLAR_BEAR: {
				return Material.POLAR_BEAR_SPAWN_EGG;
			}
			case PUFFERFISH: {
				return Material.PUFFERFISH_SPAWN_EGG;
			}
			case RABBIT: {
				return Material.RABBIT_SPAWN_EGG;
			}
			case SALMON: {
				return Material.SALMON_SPAWN_EGG;
			}
			case SHEEP: {
				return Material.SHEEP_SPAWN_EGG;
			}
			case SHULKER: {
				return Material.SHULKER_SPAWN_EGG;
			}
			case SILVERFISH: {
				return Material.SILVERFISH_SPAWN_EGG;
			}
			case SKELETON: {
				return Material.SKELETON_SPAWN_EGG;
			}
			case SKELETON_HORSE: {
				return Material.SKELETON_HORSE_SPAWN_EGG;
			}
			case SLIME: {
				return Material.SLIME_SPAWN_EGG;
			}
			case SPIDER: {
				return Material.SPIDER_SPAWN_EGG;
			}
			case SQUID: {
				return Material.SQUID_SPAWN_EGG;
			}
			case STRAY: {
				return Material.STRAY_SPAWN_EGG;
			}
			case TROPICAL_FISH: {
				return Material.TROPICAL_FISH_SPAWN_EGG;
			}
			case TURTLE: {
				return Material.TURTLE_SPAWN_EGG;
			}
			case VEX: {
				return Material.VEX_SPAWN_EGG;
			}
			case VILLAGER: {
				return Material.VILLAGER_SPAWN_EGG;
			}
			case VINDICATOR: {
				return Material.VINDICATOR_SPAWN_EGG;
			}
			case WITCH: {
				return Material.WITCH_SPAWN_EGG;
			}
			case WITHER_SKELETON: {
				return Material.WITHER_SKELETON_SPAWN_EGG;
			}
			case WOLF: {
				return Material.WOLF_SPAWN_EGG;
			}
			case ZOMBIE: {
				return Material.ZOMBIE_SPAWN_EGG;
			}
			case ZOMBIE_HORSE: {
				return Material.ZOMBIE_HORSE_SPAWN_EGG;
			}
			// for ZOMBIE_PIGMAN see PIG_ZOMBIE
			case ZOMBIE_VILLAGER: {
				return Material.ZOMBIE_VILLAGER_SPAWN_EGG;
			}
			default: {
				return null;
			}
		}
	}
}
